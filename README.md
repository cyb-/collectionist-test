# README

## Installation

Clone the project

```
cd collectionist
bundle install
```

### First installation

Run `rails db:setup` to create and seed the database.


## Configuration

Create your `.env` and `config/database.yml` files.


## Tests

#### First time :

    rails db:migrate RAILS_ENV=test

Running tests suite :

    bundle exec rspec
