# frozen_string_literal: true

module ApplicationCable
  # Application cable channel
  class Channel < ActionCable::Channel::Base
  end
end
