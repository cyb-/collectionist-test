# frozen_string_literal: true

module ApplicationCable
  # Application cable connection
  class Connection < ActionCable::Connection::Base
  end
end
