# frozen_string_literal: true

# Base application job
class ApplicationJob < ActiveJob::Base
end
