# frozen_string_literal: true

# Shop model
class Shop < ApplicationRecord
  validates :name, presence: true, uniqueness: { case_sensitive: false }
end
