# frozen_string_literal: true

# Be sure to restart your server when you modify this file.

Rails.application.configure do
  config.generators do |g|
    g.fixture_replacement :factory_bot, dir: 'spec/factories'
    g.test_framework :rspec,
                     fixtures: true,
                     view_specs: false,
                     helper_specs: true,
                     routing_specs: false,
                     controller_specs: false,
                     request_specs: true

    g.template_engine :erb
    g.helper          false
    g.assets          false
    g.javascripts     false
    g.stylesheets     false
  end
end
