class EnableExtensions < ActiveRecord::Migration[5.2]
  def up
    enable_extension 'hstore'
    enable_extension 'unaccent'
  end

  def down
    disable_extension 'unaccent'
    disable_extension 'hstore'
  end
end
