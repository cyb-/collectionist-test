FactoryBot.define do
  factory :shop do
    sequence(:name) { |n| "#{Faker::Company.name} #{n}" }
  end
end
