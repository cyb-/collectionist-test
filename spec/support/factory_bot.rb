# frozen_string_literal: true

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods

  config.before(:suite) do
    begin # rubocop:disable Style/RedundantBegin
      DatabaseCleaner.start
      # Test factories in spec/factories are working.
      factories_to_lint = FactoryBot.factories.reject do |factory|
        factory.name =~ /^abstract_/ || factory.name =~ /^old_/
      end
      FactoryBot.lint factories_to_lint
    ensure
      DatabaseCleaner.clean
    end
  end
end
